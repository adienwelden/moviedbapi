const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
  target: 'node',

  externals: [nodeExternals()],

  node: {
    __dirname: true,
  },

  entry: path.resolve(__dirname, 'src', 'server.js'),

  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'server.bundle.js',
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json'],
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
};
