module.exports = {
  verbose: true,
  testEnvironment: 'node',
  moduleFileExtensions: ['js'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  testMatch: ['<rootDir>/(src/*.test.(js))', '**/__tests__/**/*.[jt]s?(x)'],
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
};
