FROM node:8.16.0

ENV APP_PATH=$HOME/app/desafioqd/
ENV PORT=8463

RUN echo $HOME

WORKDIR $APP_PATH

COPY package*.json ./
RUN npm install
COPY ./ .

EXPOSE 8463

CMD ["npm", "start"]