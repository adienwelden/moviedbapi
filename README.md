Consome a API [Movie DB](https://www.themoviedb.org/documentation/api?language=pt-BR)

## Objetivo 🎯

Consumir uma outra API de filmes conhecida como [Movie DB](https://www.themoviedb.org/documentation/api?language=pt-BR).

API deverá ser capaz de extrair algumas informações listadas abaixo e demonstrar informações sobre o comportamento de um determinado usuário. Tudo isso com base em chamadas e respostas de uma API que se comunicam através de JSON.

## O que deverá ser construído? 🏗

Aplicação deverá ser estruturada da seguinte forma:

1. Utilizar a API do [Movie DB](https://www.themoviedb.org/documentation/api?language=pt-BR) para consulta de dados.
2. Utilizar um banco de dados para persistir dados do usuário (Criação, Login).

E com isso, a aplicação deverá atender os seguintes critérios:

1. como USUÁRIO, quero buscar filmes por título e/ou nome de autor. (e.g. se procurar por Dory, podem ter filmes e nomes de autores)
2. como USUÁRIO, quero poder favoritar filmes. (Persistir essas informações na tabela de Usuário do seu banco de dados escolhido.)
3. como USUÁRIO, quero poder ver um histórico dos filmes que pretendo assistir. (Persistir essas informações na tabela de Usuário do seu banco de dados escolhido.)
4. como USUÁRIO, quero poder ver um histórico dos filmes que já assisti. (Persistir essas informações na tabela de Usuário do seu banco de dados escolhido.)
5. como USUÁRIO, quero poder ver um feed de filmes categorizados por gênero e na seguinte ordem: Meus Favoritos, Lançamentos, Ação, Aventura, Terror.


## Comandos

```
$ npm install
$ npm start
$ npm run test
```

## Dependências

- Framework do backend: `node`, `express`, `body-parser`, `nodemon`, `webpack`, `babel`.
- Validação das requisições: `express-validator`.
- Requisições: `axios`, `query-string`.
- Autenticação: `jsonwebtoken`, `bcrypt`.
- Banco de Dados: `MongoDB`, `mongoose`, `mongoose-paginate-v2`.
- Testes: `jest`, `chai`, `supertest`, `cuid`.
- Padronização código: `eslint`, `prettier`.

## Funcionalidades/Rotas 🏗

- POST /auth/cadastro
  - recebe parametros email e password e retorna objeto usuário.
- POST /auth/login
  - recebe parametros email e password e retorna token de autenticação.
- DELETE /auth/cadastro
  - retorna objeto usuário deletado.
- GET /filmes/buscar/?query={query}
  - retorna lista paginada de filmes por título e/ou nome de autor pela sring {query}
- GET /filmes/feed
  - retorna um feed de filmes ordenados por [Favoritos, Lançamentos, +generos]
- GET /filmes/assistidos
  - retorna lista paginada de filmes assistidos
- GET /filmes/assistirei
  - retorna lista paginada de filmes a assistir
- GET /filmes/favoritar/{id}
  - - retorna o status da ação favoritar filme.
- GET /filmes/assistidos/{id}
  - - retorna o status da ação marcar como assistido.
- GET /filmes/assistirei/{id}
  - retorna o status da ação marcar como "quero assistir".
- GET /filmes/favoritar/{id}/?a=exc
  - - retorna o status da ação desfavoritar filme.
- GET /filmes/assistidos/{id}/?a=exc
  - - retorna o status da ação desmarcar como assistido.
- GET /filmes/assistirei/{id}/?a=exc
  - retorna o status da ação desmarcar como "quero assistir".

## Características

- Cache de consultas
- Testes unitários/integração
- Virtualização do ambiente de dev utilizando Docker

## Estrutura

```
+-- <projeto>/
|	+-- src/
|	|	+-- __tests__/
|	|	|	+-- features/
|	|	|	|   +-- auth.test.js
|	|	|	|   +-- feed.test.js
|	|	|	|   +-- lists.test.js
|	|	|	|   +-- search.test.js
|	|	|	+-- repos/
|	|	|	|   +-- movie.test.js
|	|	|	|   +-- user.test.js
|	|	+-- api/
|	|	|	+-- mongoDB.js
|	|	|	+-- movieDB.js
|	|	+-- controllers/
|	|	|	+-- auth.js
|	|	|	+-- movie.js
|	|	+-- features/
|	|	|	+-- auth.js
|	|	|	+-- feed.js
|	|	|	+-- lists.js
|	|	|	+-- search.js
|	|	+-- models/
|	|	|	+-- movie.js
|	|	|	+-- movieSchema.js
|	|	|	+-- user.js
|	|	|	+-- userSchema.js
|	|	+-- repos/
|	|	|	+-- movie.js
|	|	|	+-- user.js
|	|	+-- routes/
|	|	|	+-- auth.js
|	|	|	+-- movie.js
|	|	+-- utils/
|	|	|	+-- utils.js
|	|	+-- server.js
|	+-- .babelrc
|	+-- .dockerignore
|	+-- .editorconfig
|	+-- .eslintrc
|	+-- .gitignore
|	+-- .prettierrc
|	+-- config.js
|	+-- cors.json
|	+-- Dockerfile
|	+-- index.js
|	+-- jest.config.js
|	+-- package-lock.json
|	+-- package.json
|	+-- README.md
|	+-- webpack.config.js
```

## Como está implementado

O banco ( `MongoDB/mongoose` ) contém o modelo Usuarios que guarda as listas pessoais (arrays) com IDs dos filmes assistidos, a assistir e favoritos.

A ação de favoritar|assistir|pretender, e seus inversos, manipulam essas respectivas listas no usuário. Incluindo ou excluindo IDs de filmes de acordo com a rota e o parâmetro - não obrigatório - `a` ('inc'|'exc'). A ausência do parâmetro equivale ação de inclusão.

Para que a requisição dessas listas possa ser eficiente, em cada ação de inclusão as informações solicitadas da Movie DB são mantidas num quarto array do modelo Usuario, chamado filmes ( há um mero filtro dos campos relevantes ).

Então temos os arrays/listas:
favoritos `[Number]`
assistidos `[Number]`
assistir `[Number]` e
filmes `[Filme]`

Aonde o pedido duma lista pessoal simplesmente faz a interseção da lista de "filmes" com essa lista pessoal, devolvendo os filmes.

Outra vantagem dessa forma é que, quando se vai incluir um filme já incluso noutra lista, não é necessário fazer nova requisição à API consumida. Porque ele já está no cache local do usuário (filmes) e só será excluído quando a última referência a ele for excluída.

Portanto, um requisito no código para acionar ( `axios` ) a Movie DB durante inclusão de filmes é ele estar ausente no cache pessoal do usuário ( `usuario.filmes.find()` ). Mas não só, como veremos abaixo.

Diminuir mais a latência requer um cache mais geral.

Para isso foi definido um outro modelo no banco, de Filmes, que serve todos usuários.

Esse repositório Filmes é alimentado pela primeira busca, de qualquer usuário, feita pelo feed com o cache geral vazio. Afinal, diferentemente da pesquisa ( e obviamente das listas pessoais ), o feed é igual para todos.

Este resultado trazido da Movie DB é mantido neste modelo com exceção da categoria Lançamentos, pela hipótese de que a mudança nessa categoria é contínua.

Então o feed funciona assim:

O cache geral está vazio no modelo Filmes. Um usuário U1 faz a primeira requisição ao feed da aplicação.
Primeiramente seus filmes favoritos já estão no cache pessoal, como dito acima, e nenhuma requisição é feita para isso.
Em seguida a Movie DB é necessariamente consumida para trazer os últimos lançamentos e nenhum cache é feito.
Finalmente as categorias também são buscadas e como suas variabilidades são baixas, todos esses resultados vão para o cache geral com os respectivos flags indicando a categoria. Essas ações podem e são executadas todas em paralelo.

Quando um outro usuário U2 requisita o feed, a Movie BD é consumida para os lançamentos e todas as categorias restantes são obtidas do banco. A diferença de tempo pode ser observada na saída dos testes.

Finalizando a minimização da latência, quando o usuário requisita inclusão de um filme que não está no seu cache pessoal, ele ainda é procurado no cache geral das categorias. Caso se encontre aí, bingo! O filme é replicado e passa fazer parte também do seu cache local.

Bom observar que ficou fora do escopo do desafio implementar um gerenciamento desse cache geral. Ele precisa ser apagado manualmente.

A característica maior aqui é o grau de generalidade. As categorias a serem buscadas no feed podem ser em qualquer ordem e qualquer quantidade delas. Não apenas as definidas no desafio, que estão como o padrão nas configurações da API, especificadas no campo `categoriasOrdem`: um array de IDs de categorias, nesta ordem. Uma referência dos IDs se encontra em `categoriasFilme` no arquivo /src/utils/utils.js. Outra configuração relacionada é o tamanho do feed.

O tamanho padrão do feed está em `/config.js` e é a quantidade de filmes retornada em cada uma das categorias. Mas também pode ser enviado via url no parâmetro `categ_size`.

Já a busca por string em GET /filmes/buscar faz um "by pass" dos parâmetros ( `query-string` ) aceitos pela Movie DB. Aqui não há algum cache dessas consultas pelo alto grau de imprevisibilidade.

No mais, foi implementada paginação do cache geral utilizando um plugin do mongoose ( `mongoose-paginate-v2` ) e paginação do cache pessoal ( um array de mongoose Schema ) em vanilla javascript.

As senhas são codificadas ( `bcrypt` ) e todas as rotas autenticadas por um token de duração limitada ( `jsonwebtoken` ).

Os parâmetros obrigatórios são validados ( `express-validator` ) e as respostas estão num objeto de formato
`{ ok: [Boolean], payload: [Any], }`.

Os testes são rodados num banco separado e contemplam as alterações nas configurações de categorias do feed.

Finalmente o arquivo docker criado permite a virtualização do ambiente.

Se permaneceu alguma dúvida ou para mais detalhes sobre a solução favor entrar em contato com o autor.

## Algumas melhorias

- Escrever uma documentação desta API.
- Aprofundar os testes visando situações atípicas.
- Implementar um mecanismo de gerenciamento do cache geral.
- ...
