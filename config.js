// eslint-disable-next-line operator-linebreak
const mongoDB =
  process.env.NODE_ENV === 'test'
    ? 'mongodb://localhost/testeQDTest'
    : 'mongodb://localhost/testeQD';

const serverConfig = {
  apiHost: 'http://localhost',
  apiPort: process.env.PORT || 8463,
  movieDB: 'https://api.themoviedb.org/3',
  movieDBApiKey: '',
  mongoDB,
  tokenSecret: 'eyJhdWQiOiI5MWNjMDM4YTVmNjY3YzgxN',
  tamanhoFeed: 2,
  categoriasOrdem: [12, 28, 27],
};

export default serverConfig;
