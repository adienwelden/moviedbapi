import * as feed from '../features/feed';
import * as listas from '../features/lists';
import * as busca from '../features/search';

export { feed, listas, busca };
