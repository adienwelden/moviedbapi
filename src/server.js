import http from 'http';
import Express from 'express';
import bodyParser from 'body-parser';
import expressValidator from 'express-validator';

import cors from '../cors.json';
import serverConfig from '../config';

import authRoutes from './routes/auth';
import moviesRoutes from './routes/movies';

const app = new Express();

app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: false }));
app.use(expressValidator());

app.use((req, res, next) => {
  res.header(cors);
  next();
});

app.use('/auth', authRoutes);
app.use('/filmes', moviesRoutes);

const httpServer = http.createServer(app);

if (process.env.NODE_ENV !== 'test') {
  httpServer.listen(serverConfig.apiPort, () => {});
}

export default httpServer;
