import cuid from 'cuid';
import { assert } from 'chai';
import request from 'supertest';

import serverConfig from '../../../config';
import httpServer from '../../server';

import { filmes as bancoFilmes } from '../../api/mongoDB';
import { filmes } from '../../utils/utils';

const tam = serverConfig.tamanhoFeed;
const usuario1 = {
  email: `${cuid()}@dominio.com.br`,
  password: '012345',
};
const usuario2 = {
  email: `${cuid()}@dominio.com.br`,
  password: '123456',
};

// eslint-disable-next-line no-undef
describe('Testa o feed (favoritos, lançamentos, +generos):', () => {
  let token;
  let feed;
  let algumFilmeIdNoCacheGeral;

  // eslint-disable-next-line no-undef
  it('POST /auth/cadastro: cadastra usuário teste.', async () => {
    await request(httpServer)
      .post('/auth/cadastro')
      .send(usuario1)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.exists(payload.email, 'Não retornou campo <email> na resposta.');
        assert.exists(
          payload.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          payload.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          payload.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.equal(
          payload.email,
          usuario1.email,
          `Email ${payload.email} não corresponde ao cadastrado ${
            usuario1.email
          }.`,
        );
        return payload;
      });
  });

  // eslint-disable-next-line no-undef
  it('POST /auth/login: loga usuário teste.', async () => {
    token = await request(httpServer)
      .post('/auth/login')
      .send(usuario1)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.token, 'Não retornou campo <token> na resposta.');
        assert.isString(payload.token, 'Campo <token> não é um string.');
        assert.equal(
          payload.token.split('.').length,
          3,
          'Formato do token não bate.',
        );
        return response.body.payload.token;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/favoritar/{id}: favoritar filme 1.', async () => {
    await request(httpServer)
      .get(`/filmes/favoritar/${filmes[5]}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.id,
          'Não retornou campo <payload.id> na resposta.',
        );
        assert.exists(
          payload.msg,
          'Não retornou campo <payload.msg> na resposta.',
        );
        assert.equal(
          payload.id,
          filmes[5],
          'Não houve retorno correto do id do filme favoritado',
        );
        return payload.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/favoritar/{id}: favoritar filme 2.', async () => {
    await request(httpServer)
      .get(`/filmes/favoritar/${filmes[6]}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.id,
          'Não retornou campo <payload.id> na resposta.',
        );
        assert.exists(
          payload.msg,
          'Não retornou campo <payload.msg> na resposta.',
        );
        assert.equal(
          payload.id,
          filmes[6],
          'Não houve retorno correto do id do filme favoritado',
        );
        return payload.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/feed: lista categorizada por gênero.', async () => {
    feed = await request(httpServer)
      .get('/filmes/feed')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isArray(payload, 'Campo <payload> não é um array.');
        assert.notEqual(payload.length, 0, 'A lista de resultados está vazia.');
        return payload;
      });
  }, 10000);

  // eslint-disable-next-line no-undef
  it('verifica ordenação do feed [favoritos, lançamentos, +generos].', () => {
    if (feed && Array.isArray(feed)) {
      const favoritos = feed.slice(0, 2);
      const diffFavoritados = favoritos.find(
        item => item.id !== filmes[5] && item.id !== filmes[6],
      );
      assert.isUndefined(
        diffFavoritados,
        'Categoria favoritos contém filme não favoritado.',
      );

      const lancamentos = feed.slice(2, 2 + tam);
      const dif2019 = lancamentos.find(
        item => item.release_date.slice(0, 4) !== '2019',
      );
      assert.isUndefined(
        dif2019,
        'Categoria lançamentos contém filme != 2019.',
      );

      const ordenados = feed.slice(2 + tam);
      const desordenado = ordenados.find((item, key) => {
        const indexOrdem = (key - (key % tam)) / tam;
        const categoriaAtual = serverConfig.categoriasOrdem[indexOrdem];
        const continuaOrdenado = item.genre_ids.find(
          categ => categ === categoriaAtual,
        );
        if (!continuaOrdenado) return true;
        return false;
      });

      assert.isUndefined(
        desordenado,
        'As categorias não estão na ordem [ação, aventura, terror].',
      );
      algumFilmeIdNoCacheGeral = ordenados[ordenados.length - 1].id;
    } else {
      assert.isArray(feed, 'Não há uma lista de feed.');
    }
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/feed: lista categorizada por gênero deve ser mais rápido.', async () => {
    feed = await request(httpServer)
      .get('/filmes/feed')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isArray(payload, 'Campo <payload> não é um array.');
        assert.notEqual(payload.length, 0, 'A lista de resultados está vazia.');
        return payload;
      });
  }, 5000);

  // eslint-disable-next-line no-undef
  it('verifica ordenação do feed [favoritos, lançamentos, +generos].', () => {
    if (feed && Array.isArray(feed)) {
      const favoritos = feed.slice(0, 2);
      const diffFavoritados = favoritos.find(
        item => item.id !== filmes[5] && item.id !== filmes[6],
      );
      assert.isUndefined(
        diffFavoritados,
        'Categoria favoritos contém filme não favoritado.',
      );

      const lancamentos = feed.slice(2, 2 + tam);
      const dif2019 = lancamentos.find(
        item => item.release_date.slice(0, 4) !== '2019',
      );
      assert.isUndefined(
        dif2019,
        'Categoria lançamentos contém filme != 2019.',
      );

      const ordenados = feed.slice(2 + tam);
      const desordenado = ordenados.find((item, key) => {
        const indexOrdem = (key - (key % tam)) / tam;
        const categoriaAtual = serverConfig.categoriasOrdem[indexOrdem];
        const continuaOrdenado = item.genre_ids.find(
          categ => categ === categoriaAtual,
        );
        if (!continuaOrdenado) return true;
        return false;
      });

      assert.isUndefined(
        desordenado,
        'As categorias não estão na ordem [ação, aventura, terror].',
      );
    } else {
      assert.isArray(feed, 'Não há uma lista de feed.');
    }
  });

  // eslint-disable-next-line no-undef
  it('DELETE /auth/cadastro: deleta o usuário criado.', async () => {
    await request(httpServer)
      .delete('/auth/cadastro')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          payload.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          payload.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.equal(
          payload.email,
          usuario1.email,
          `Email ${payload.email} não corresponde ao cadastrado ${
            usuario1.email
          }.`,
        );
      });
  });

  // eslint-disable-next-line no-undef
  it('POST /auth/cadastro: cadastra outro usuário.', async () => {
    await request(httpServer)
      .post('/auth/cadastro')
      .send(usuario2)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.exists(payload.email, 'Não retornou campo <email> na resposta.');
        assert.exists(
          payload.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          payload.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          payload.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.equal(
          payload.email,
          usuario2.email,
          `Email ${payload.email} não corresponde ao cadastrado ${
            usuario2.email
          }.`,
        );
        return payload;
      });
  });

  // eslint-disable-next-line no-undef
  it('POST /auth/login: loga este usuário cadastrado.', async () => {
    token = await request(httpServer)
      .post('/auth/login')
      .send(usuario2)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.token, 'Não retornou campo <token> na resposta.');
        assert.isString(payload.token, 'Campo <token> não é um string.');
        assert.equal(
          payload.token.split('.').length,
          3,
          'Formato do token não bate.',
        );
        return response.body.payload.token;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/favoritar/{id}: inclusão de filme presente no cache geral deve ser mais rápido.', async () => {
    await request(httpServer)
      .get(`/filmes/favoritar/${algumFilmeIdNoCacheGeral}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.id,
          'Não retornou campo <payload.id> na resposta.',
        );
        assert.exists(
          payload.msg,
          'Não retornou campo <payload.msg> na resposta.',
        );
        assert.equal(
          payload.id,
          algumFilmeIdNoCacheGeral,
          'Não houve retorno correto do id do filme favoritado',
        );
        return payload.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/feed: lista categorizada por gênero deve ser rápido.', async () => {
    feed = await request(httpServer)
      .get('/filmes/feed')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isArray(payload, 'Campo <payload> não é um array.');
        assert.notEqual(payload.length, 0, 'A lista de resultados está vazia.');
        return payload;
      });
  }, 5000);

  // eslint-disable-next-line no-undef
  it('verifica ordenação do feed [favoritos, lançamentos, +generos].', () => {
    if (feed && Array.isArray(feed)) {
      const favoritos = feed.slice(0, 1);
      const diffFavoritados = favoritos.find(
        item => item.id !== algumFilmeIdNoCacheGeral,
      );
      assert.isUndefined(
        diffFavoritados,
        'Categoria favoritos contém filme não favoritado.',
      );

      const lancamentos = feed.slice(1, 1 + tam);
      const dif2019 = lancamentos.find(
        item => item.release_date.slice(0, 4) !== '2019',
      );
      assert.isUndefined(
        dif2019,
        'Categoria lançamentos contém filme != 2019.',
      );

      const ordenados = feed.slice(1 + tam);
      const desordenado = ordenados.find((item, key) => {
        const indexOrdem = (key - (key % tam)) / tam;
        const categoriaAtual = serverConfig.categoriasOrdem[indexOrdem];
        const continuaOrdenado = item.genre_ids.find(
          categ => categ === categoriaAtual,
        );
        if (!continuaOrdenado) return true;
        return false;
      });

      assert.isUndefined(
        desordenado,
        'As categorias não estão na ordem [ação, aventura, terror].',
      );
    } else {
      assert.isArray(feed, 'Não há uma lista de feed.');
    }
  });

  // eslint-disable-next-line no-undef
  it('deleta o cache geral.', async () => {
    await bancoFilmes.deletaTodosFilmes().then(ret => {
      assert.isString(ret, 'Resposta não é um string.');
      assert.equal(
        ret,
        'Todos filmes foram deletados.',
        'Mensagem não confirma todos foram deletados.',
      );
    });
  });

  // eslint-disable-next-line no-undef
  it('confere todos filmes foram deletados.', async () => {
    await bancoFilmes.leFilmes().then(lido => {
      assert.isObject(lido, 'Campo <payload> não é um objeto.');
      assert.exists(lido.page, 'Não retornou campo <page> na resposta.');
      assert.exists(
        lido.totalDocs,
        'Não retornou campo <totalDocs> na resposta.',
      );
      assert.exists(
        lido.totalPages,
        'Não retornou campo <totalPages> na resposta.',
      );
      assert.exists(lido.docs, 'Não retornou campo <docs> na resposta.');
      assert.equal(lido.totalPages, 1, 'Número de páginas nao é 1.');
      assert.equal(lido.page, 1, 'A página nao é 1.');
      assert.isArray(lido.docs, 'Campo <docs> não é um array.');
      assert.equal(lido.totalDocs, 0, 'A lista não contém zero resultados.');
      assert.equal(lido.docs.length, 0, 'A lista não contém zero itens.');
    });
  });

  // eslint-disable-next-line no-undef
  it('DELETE /auth/cadastro: deleta o usuário criado.', async () => {
    await request(httpServer)
      .delete('/auth/cadastro')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          payload.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          payload.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.equal(
          payload.email,
          usuario2.email,
          `Email ${payload.email} não corresponde ao cadastrado ${
            usuario2.email
          }.`,
        );
      });
  });
});
