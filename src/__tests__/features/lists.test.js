import cuid from 'cuid';
import { assert } from 'chai';
import request from 'supertest';

import httpServer from '../../server';
import { filmes } from '../../utils/utils';

const usuario = {
  email: `${cuid()}@dominio.coms.br`,
  password: '123456',
};

// eslint-disable-next-line no-undef
describe('Teste de favoritados | assistidos | pretendidos:', () => {
  let token;

  // eslint-disable-next-line no-undef
  it('POST /auth/cadastro: cadastra usuário teste.', async () => {
    await request(httpServer)
      .post('/auth/cadastro')
      .send(usuario)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.exists(payload.email, 'Não retornou campo <email> na resposta.');
        assert.exists(
          payload.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          payload.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          payload.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.equal(
          payload.email,
          usuario.email,
          `Email ${payload.email} não corresponde ao cadastrado ${
            usuario.email
          }.`,
        );
        return payload;
      });
  });

  // eslint-disable-next-line no-undef
  it('POST /auth/login: loga usuário teste.', async () => {
    token = await request(httpServer)
      .post('/auth/login')
      .send(usuario)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.token, 'Não retornou campo <token> na resposta.');
        assert.isString(payload.token, 'Campo <token> não é um string.');
        assert.equal(
          payload.token.split('.').length,
          3,
          'Formato do token não bate.',
        );
        return response.body.payload.token;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistidos: lista de assistidos deve estar vazia.', async () => {
    await request(httpServer)
      .get('/filmes/assistidos')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.page, 'Não retornou campo <page> na resposta.');
        assert.exists(
          payload.total_results,
          'Não retornou campo <total_results> na resposta.',
        );
        assert.exists(
          payload.total_pages,
          'Não retornou campo <total_pages> na resposta.',
        );
        assert.exists(
          payload.results,
          'Não retornou campo <results> na resposta.',
        );
        assert.equal(payload.results.length, 0, 'A lista não está vazia.');
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistirei: lista de pretendidos deve estar vazia.', async () => {
    await request(httpServer)
      .get('/filmes/assistirei')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.page, 'Não retornou campo <page> na resposta.');
        assert.exists(
          payload.total_results,
          'Não retornou campo <total_results> na resposta.',
        );
        assert.exists(
          payload.total_pages,
          'Não retornou campo <total_pages> na resposta.',
        );
        assert.exists(
          payload.results,
          'Não retornou campo <results> na resposta.',
        );
        assert.equal(payload.results.length, 0, 'A lista não está vazia.');
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/favoritar/{id}: favorita um filme válido.', async () => {
    await request(httpServer)
      .get(`/filmes/favoritar/${filmes[0]}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.id,
          'Não retornou campo <payload.id> na resposta.',
        );
        assert.exists(
          payload.msg,
          'Não retornou campo <payload.msg> na resposta.',
        );
        assert.equal(
          payload.id,
          filmes[0],
          'Não houve retorno correto do id do filme favoritado',
        );
        return payload.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/favoritar/{id}: não favorita filme favoritado.', async () => {
    await request(httpServer)
      .get(`/filmes/favoritar/${filmes[0]}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(400)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, false, 'Status OK da resposta = true.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um array.');
        assert.equal(payload.id, filmes[0], 'Não se refere ao id do filme.');
        assert.equal(
          payload.msg,
          'Filme já existe na lista favoritos.',
          'Resposta não confere.',
        );
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistidos/{id}: marcar o anterior como assistido.', async () => {
    await request(httpServer)
      .get(`/filmes/assistidos/${filmes[0]}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.id,
          'Não retornou campo <payload.id> na resposta.',
        );
        assert.exists(
          payload.msg,
          'Não retornou campo <payload.msg> na resposta.',
        );
        assert.equal(
          payload.id,
          filmes[0],
          'Não houve retorno correto do id do filme assistido',
        );
        return payload.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistidos: 1 item na lista assistidos.', async () => {
    await request(httpServer)
      .get('/filmes/assistidos')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.page, 'Não retornou campo <page> na resposta.');
        assert.exists(
          payload.total_results,
          'Não retornou campo <total_results> na resposta.',
        );
        assert.exists(
          payload.total_pages,
          'Não retornou campo <total_pages> na resposta.',
        );
        assert.exists(
          payload.results,
          'Não retornou campo <results> na resposta.',
        );
        assert.equal(
          payload.results.length,
          1,
          'A lista assistidos não contem 1 item.',
        );
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistidos/{id}: falha remarcar como assistido.', async () => {
    await request(httpServer)
      .get(`/filmes/assistidos/${filmes[0]}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(400)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, false, 'Status OK da resposta = true.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um array.');
        assert.equal(payload.id, filmes[0], 'Não se refere ao id do filme.');
        assert.equal(
          payload.msg,
          'Filme já existe na lista assistidos.',
          'Resposta não confere.',
        );
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistidos/{id}/?a=exc: desmarca como assistido.', async () => {
    await request(httpServer)
      .get(`/filmes/assistidos/${filmes[0]}/?a=exc`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.id,
          'Não retornou campo <payload.id> na resposta.',
        );
        assert.exists(
          payload.msg,
          'Não retornou campo <payload.msg> na resposta.',
        );
        assert.equal(
          payload.id,
          filmes[0],
          'Não houve retorno correto do id do filme assistido',
        );
        return payload.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistirei/{id}: marcar algum (já no cache) como pretendido.', async () => {
    await request(httpServer)
      .get(`/filmes/assistirei/${filmes[0]}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.id,
          'Não retornou campo <payload.id> na resposta.',
        );
        assert.exists(
          payload.msg,
          'Não retornou campo <payload.msg> na resposta.',
        );
        assert.equal(
          payload.id,
          filmes[0],
          'Não houve retorno correto do id do filme pretendido',
        );
        return payload.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistirei/{id}: marcar um novo (sem cache) como pretendido.', async () => {
    await request(httpServer)
      .get(`/filmes/assistirei/${filmes[6]}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.id,
          'Não retornou campo <payload.id> na resposta.',
        );
        assert.exists(
          payload.msg,
          'Não retornou campo <payload.msg> na resposta.',
        );
        assert.equal(
          payload.id,
          filmes[6],
          'Não houve retorno correto do id do filme pretendido',
        );
        return payload.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistirei: 2 itens na lista quero assistir.', async () => {
    await request(httpServer)
      .get('/filmes/assistirei')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.page, 'Não retornou campo <page> na resposta.');
        assert.exists(
          payload.total_results,
          'Não retornou campo <total_results> na resposta.',
        );
        assert.exists(
          payload.total_pages,
          'Não retornou campo <total_pages> na resposta.',
        );
        assert.exists(
          payload.results,
          'Não retornou campo <results> na resposta.',
        );
        assert.equal(
          payload.results.length,
          2,
          'A lista pretendidos não contem 1 item.',
        );
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistirei/{id}: falha remarcar quero assistir.', async () => {
    await request(httpServer)
      .get(`/filmes/assistirei/${filmes[0]}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(400)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, false, 'Status OK da resposta = true.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um object.');
        assert.equal(payload.id, filmes[0], 'Não se refere ao id do filme.');
        assert.equal(
          payload.msg,
          'Filme já existe na lista assistirei.',
          'Resposta não confere.',
        );
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/favoritar/{id}/?a=exc: desfavoritar o favoritado.', async () => {
    await request(httpServer)
      .get(`/filmes/favoritar/${filmes[0]}/?a=exc`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.id,
          'Não retornou campo <payload.id> na resposta.',
        );
        assert.exists(
          payload.msg,
          'Não retornou campo <payload.msg> na resposta.',
        );
        assert.equal(
          payload.id,
          filmes[0],
          'Não houve retorno correto do id do filme favoritado',
        );
        return payload.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistidos/{id}: marcar o anterior como assistido.', async () => {
    await request(httpServer)
      .get(`/filmes/assistidos/${filmes[0]}`)
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.id,
          'Não retornou campo <payload.id> na resposta.',
        );
        assert.exists(
          payload.msg,
          'Não retornou campo <payload.msg> na resposta.',
        );
        assert.equal(
          payload.id,
          filmes[0],
          'Não houve retorno correto do id do filme assistido',
        );
        return payload.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('DELETE /auth/cadastro: deleta o usuário de teste.', async () => {
    await request(httpServer)
      .delete('/auth/cadastro')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          payload.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          payload.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.equal(
          payload.email,
          usuario.email,
          `Email ${payload.email} não corresponde ao cadastrado ${
            usuario.email
          }.`,
        );
      });
  });
});
