import cuid from 'cuid';
import { assert } from 'chai';
import request from 'supertest';

import httpServer from '../../server';

const usuario = {
  email: `${cuid()}@dominio.org.br`,
  password: '123456',
};

// eslint-disable-next-line no-undef
describe('Teste das rotas de autenticação:', () => {
  let token;

  // eslint-disable-next-line no-undef
  it('POST /auth/cadastro: envia credenciais e recebe objeto usuario cadastrado.', async () => {
    await request(httpServer)
      .post('/auth/cadastro')
      .send(usuario)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.exists(payload.email, 'Não retornou campo <email> na resposta.');
        assert.exists(
          payload.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          payload.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          payload.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.exists(
          payload.filmes,
          'Não retornou campo <filmes> na resposta.',
        );
        assert.equal(
          payload.email,
          usuario.email,
          `Email ${payload.email} não corresponde ao cadastrado ${
            usuario.email
          }.`,
        );
        return payload;
      });
  });

  // eslint-disable-next-line no-undef
  it('POST /auth/cadastro: não pode cadastrar mesmo usuário.', async () => {
    await request(httpServer)
      .post('/auth/cadastro')
      .send(usuario)
      .expect('Content-Type', /json/)
      .expect(400)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, false, 'Status OK da resposta = true.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.equal(
          payload,
          'Email já está cadastrado.',
          'Resposta não esperada.',
        );
      });
  });

  // eslint-disable-next-line no-undef
  it('POST /auth/login: envia credenciais e recebe token.', async () => {
    token = await request(httpServer)
      .post('/auth/login')
      .send(usuario)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.token, 'Não retornou campo <token> na resposta.');
        assert.isString(payload.token, 'Campo <token> não é um string.');
        assert.equal(
          payload.token.split('.').length,
          3,
          'Formato do token não bate.',
        );
        return response.body.payload.token;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistidos: sem acesso rota autenticada com token inválido.', async () => {
    await request(httpServer)
      .get('/filmes/assistidos')
      .set('Authorization', `Bearer ${token}lixo`)
      .expect('Content-Type', /json/)
      .expect(401)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, false, 'Status OK da resposta = true.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isString(payload, 'Campo <payload> não é um string de erro.');
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/assistidos: acessa rota autenticada com token válido.', async () => {
    await request(httpServer)
      .get('/filmes/assistidos')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.page, 'Não retornou campo <page> na resposta.');
      });
  });

  // eslint-disable-next-line no-undef
  it('DELETE /auth/cadastro: deleta o usuário cadastrado.', async () => {
    await request(httpServer)
      .delete('/auth/cadastro')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          payload.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          payload.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.equal(
          payload.email,
          usuario.email,
          `Email ${payload.email} não corresponde ao cadastrado ${
            usuario.email
          }.`,
        );
        httpServer.close();
      });
  });
});
