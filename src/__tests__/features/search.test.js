import cuid from 'cuid';
import { assert } from 'chai';
import request from 'supertest';

import httpServer from '../../server';

const usuario = {
  email: `${cuid()}@dominio.coms.br`,
  password: '123456',
};

// eslint-disable-next-line no-undef
describe('Teste de busca ( string = "sim"):', () => {
  let token;

  // eslint-disable-next-line no-undef
  it('POST /auth/cadastro: cadastra usuário teste.', async () => {
    await request(httpServer)
      .post('/auth/cadastro')
      .send(usuario)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.exists(payload.email, 'Não retornou campo <email> na resposta.');
        assert.exists(
          payload.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          payload.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          payload.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.equal(
          payload.email,
          usuario.email,
          `Email ${payload.email} não corresponde ao cadastrado ${
            usuario.email
          }.`,
        );
        return payload;
      });
  });

  // eslint-disable-next-line no-undef
  it('POST /auth/login: loga usuário teste.', async () => {
    token = await request(httpServer)
      .post('/auth/login')
      .send(usuario)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.token, 'Não retornou campo <token> na resposta.');
        assert.isString(payload.token, 'Campo <token> não é um string.');
        assert.equal(
          payload.token.split('.').length,
          3,
          'Formato do token não bate.',
        );
        return response.body.payload.token;
      });
  });

  // eslint-disable-next-line no-undef
  it('GET /filmes/buscar/?query={query}: faz uma busca ( string "sim" ).', async () => {
    await request(httpServer)
      .get('/filmes/buscar/?query=sim')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(payload.page, 'Não retornou campo <page> na resposta.');
        assert.exists(
          payload.total_results,
          'Não retornou campo <total_results> na resposta.',
        );
        assert.exists(
          payload.total_pages,
          'Não retornou campo <total_pages> na resposta.',
        );
        assert.exists(
          payload.results,
          'Não retornou campo <results> na resposta.',
        );
        assert.isArray(payload.results, 'Campo <results> não é um array.');
        assert.notEqual(
          payload.results.length,
          0,
          'A lista de resultados está vazia.',
        );
      });
  }, 10000);

  // eslint-disable-next-line no-undef
  it('DELETE /auth/cadastro: deleta o usuário de teste.', async () => {
    await request(httpServer)
      .delete('/auth/cadastro')
      .set('Authorization', `Bearer ${token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        const { ok, payload } = response.body;
        assert.exists(ok, 'Não retornou campo <ok> na resposta.');
        assert.equal(ok, true, 'Status OK da resposta = false.');
        assert.exists(payload, 'Não retornou campo <payload> na resposta.');
        assert.isObject(payload, 'Campo <payload> não é um objeto.');
        assert.exists(
          payload.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          payload.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          payload.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.equal(
          payload.email,
          usuario.email,
          `Email ${payload.email} não corresponde ao cadastrado ${
            usuario.email
          }.`,
        );
      });
  });
});
