/* eslint-disable no-underscore-dangle */
import cuid from 'cuid';
import { assert } from 'chai';

import * as banco from '../../api/mongoDB';

const filmes = [
  {
    adult: false,
    backdrop_path: cuid(),
    genre_ids: [1, 2, 3, 4, 5],
    id: 999,
    original_language: cuid(),
    original_title: cuid(),
    overview: cuid(),
    popularity: 12.1,
    release_date: '1969-01-01',
    title: cuid(),
    video: true,
    vote_average: '1.1',
    vote_count: 100,
  },
  {
    adult: false,
    backdrop_path: cuid(),
    genre_ids: [1, 2, 3, 4, 5],
    id: 372,
    original_language: cuid(),
    original_title: cuid(),
    overview: cuid(),
    popularity: 12.1,
    release_date: '1969-01-01',
    title: cuid(),
    video: true,
    vote_average: '1.1',
    vote_count: 100,
  },
];

// eslint-disable-next-line no-undef
describe('Teste da API de acesso ao banco para filmes:', () => {
  // eslint-disable-next-line no-unused-vars
  let criados = [];
  // eslint-disable-next-line no-undef
  it('cria um filme.', async () => {
    await banco.filmes.criaFilme(filmes[0]).then(ret => {
      assert.exists(ret.adult, 'Não retornou campo <adult> na resposta.');
      assert.exists(
        ret.backdrop_path,
        'Não retornou campo <backdrop_path> na resposta.',
      );
      assert.exists(
        ret.genre_ids,
        'Não retornou campo <genre_ids> na resposta.',
      );
      assert.exists(ret.id, 'Não retornou campo <id> na resposta.');
      assert.isArray(ret.genre_ids, 'Campo <genre_ids> não é um array.');
      assert.exists(
        ret.original_language,
        'Não retornou campo <original_language> na resposta.',
      );
      assert.exists(
        ret.original_title,
        'Não retornou campo <original_title> na resposta.',
      );
      assert.exists(ret.overview, 'Não retornou campo <overview> na resposta.');
      assert.exists(
        ret.popularity,
        'Não retornou campo <popularity> na resposta.',
      );
      assert.exists(
        ret.release_date,
        'Não retornou campo <release_date> na resposta.',
      );
      assert.exists(ret.title, 'Não retornou campo <title> na resposta.');
      assert.exists(ret.video, 'Não retornou campo <video> na resposta.');
      assert.exists(
        ret.vote_average,
        'Não retornou campo <vote_average> na resposta.',
      );
      assert.exists(
        ret.vote_count,
        'Não retornou campo <vote_count> na resposta.',
      );
      return ret;
    });
  });

  // eslint-disable-next-line no-undef
  it('cria outro filme.', async () => {
    await banco.filmes.criaFilme(filmes[1]).then(ret => {
      assert.exists(ret.adult, 'Não retornou campo <adult> na resposta.');
      assert.exists(
        ret.backdrop_path,
        'Não retornou campo <backdrop_path> na resposta.',
      );
      assert.exists(
        ret.genre_ids,
        'Não retornou campo <genre_ids> na resposta.',
      );
      assert.exists(ret.id, 'Não retornou campo <id> na resposta.');
      assert.isArray(ret.genre_ids, 'Campo <genre_ids> não é um array.');
      assert.exists(
        ret.original_language,
        'Não retornou campo <original_language> na resposta.',
      );
      assert.exists(
        ret.original_title,
        'Não retornou campo <original_title> na resposta.',
      );
      assert.exists(ret.overview, 'Não retornou campo <overview> na resposta.');
      assert.exists(
        ret.popularity,
        'Não retornou campo <popularity> na resposta.',
      );
      assert.exists(
        ret.release_date,
        'Não retornou campo <release_date> na resposta.',
      );
      assert.exists(ret.title, 'Não retornou campo <title> na resposta.');
      assert.exists(ret.video, 'Não retornou campo <video> na resposta.');
      assert.exists(
        ret.vote_average,
        'Não retornou campo <vote_average> na resposta.',
      );
      assert.exists(
        ret.vote_count,
        'Não retornou campo <vote_count> na resposta.',
      );
      return ret;
    });
  });

  // eslint-disable-next-line no-undef
  it('le os 2 filmes criados e confere os ids.', async () => {
    criados = await banco.filmes
      .leFilmesIds(filmes.map(i => i.id))
      .then(lido => {
        assert.isObject(lido, 'Campo <payload> não é um objeto.');
        assert.exists(lido.page, 'Não retornou campo <page> na resposta.');
        assert.exists(
          lido.totalDocs,
          'Não retornou campo <totalDocs> na resposta.',
        );
        assert.exists(
          lido.totalPages,
          'Não retornou campo <totalPages> na resposta.',
        );
        assert.exists(lido.docs, 'Não retornou campo <docs> na resposta.');
        assert.equal(lido.totalPages, 1, 'Número de páginas nao é 1.');
        assert.equal(lido.page, 1, 'A página nao é 1.');
        assert.isArray(lido.docs, 'Campo <docs> não é um array.');
        assert.equal(lido.totalDocs, 2, 'A lista não contém 2 resultados.');
        assert.equal(lido.docs.length, 2, 'A lista não contém 2 itens.');

        assert.equal(
          lido.docs[0].id,
          filmes[0].id,
          'Id lido não confere com enviado.',
        );
        assert.equal(
          lido.docs[1].id,
          filmes[1].id,
          'Id lido não confere com enviado.',
        );

        return lido.results;
      });
  });

  // eslint-disable-next-line no-undef
  it('deleta o filme 1.', async () => {
    await banco.filmes.deletaFilmeId(filmes[0].id).then(ret => {
      assert.isString(ret, 'Resposta não é um string.');
      assert.equal(
        ret,
        'Filme foi deletado.',
        'Mensagem não confirma foi deletado.',
      );
    });
  });

  // eslint-disable-next-line no-undef
  it('deleta o filme 2.', async () => {
    await banco.filmes.deletaFilmeId(filmes[1].id).then(ret => {
      assert.isString(ret, 'Resposta não é um string.');
      assert.equal(
        ret,
        'Filme foi deletado.',
        'Mensagem não confirma foi deletado.',
      );
    });
  });

  // eslint-disable-next-line no-undef
  it('confere que foram deletados.', async () => {
    await banco.filmes.leFilmesIds(filmes.map(i => i.id)).then(lido => {
      assert.isObject(lido, 'Campo <payload> não é um objeto.');
      assert.exists(lido.page, 'Não retornou campo <page> na resposta.');
      assert.exists(
        lido.totalDocs,
        'Não retornou campo <totalDocs> na resposta.',
      );
      assert.exists(
        lido.totalPages,
        'Não retornou campo <totalPages> na resposta.',
      );
      assert.exists(lido.docs, 'Não retornou campo <docs> na resposta.');
      assert.equal(lido.totalPages, 1, 'Número de páginas nao é 1.');
      assert.equal(lido.page, 1, 'A página nao é 1.');
      assert.isArray(lido.docs, 'Campo <docs> não é um array.');
      assert.equal(lido.totalDocs, 0, 'A lista não contém zero resultados.');
      assert.equal(lido.docs.length, 0, 'A lista não contém zero itens.');
    });
  });
});
