/* eslint-disable no-underscore-dangle */
import cuid from 'cuid';
import { assert } from 'chai';

import * as banco from '../../api/mongoDB';

const usuario = {
  email: `${cuid()}@dominio.coms.br`,
  password: '123456',
};

const filme = {
  adult: false,
  backdrop_path: cuid(),
  genre_ids: [1, 2, 3, 4, 5],
  id: 999,
  original_language: cuid(),
  original_title: cuid(),
  overview: cuid(),
  popularity: 12.1,
  release_date: '1969-01-01',
  title: cuid(),
  video: true,
  vote_average: '1.1',
  vote_count: 100,
};

// eslint-disable-next-line no-undef
describe('Teste da API de acesso ao banco para usuário:', () => {
  let criado;
  // eslint-disable-next-line no-undef
  it('cria um usuário aleatório.', async () => {
    criado = await banco.usuarios.criaUsuario(usuario).then(ret => {
      assert.exists(
        ret.assistirei,
        'Não retornou campo <assistirei> na resposta.',
      );
      assert.exists(
        ret.assistidos,
        'Não retornou campo <assistidos> na resposta.',
      );
      assert.exists(
        ret.favoritos,
        'Não retornou campo <favoritos> na resposta.',
      );
      assert.exists(ret.email, 'Não retornou campo <email> na resposta.');
      assert.isArray(ret.assistirei, 'Campo <assistirei> não é um array.');
      assert.isArray(ret.assistidos, 'Campo <assistidos> não é um array.');
      assert.isArray(ret.favoritos, 'Campo <favoritos> não é um array.');
      assert.isArray(ret.filmes, 'Campo <filmes> não é um array.');
      assert.equal(
        ret.assistirei.length,
        0,
        'Campo <assistirei> não está iniciado vazio.',
      );
      assert.equal(
        ret.assistidos.length,
        0,
        'Campo <assistidos> não está iniciado vazio.',
      );
      assert.equal(
        ret.favoritos.length,
        0,
        'Campo <favoritos> não está iniciado vazio.',
      );
      assert.equal(
        ret.filmes.length,
        0,
        'Campo <filmes> não está iniciado vazio.',
      );
      assert.equal(
        ret.email,
        usuario.email,
        'Email do usuario criado não confere.',
      );
      return ret;
    });
  });

  // eslint-disable-next-line no-undef
  it('edita uma lista do usuário.', async () => {
    await banco.usuarios
      .editaUsuario(criado._id, 'favoritos', [123])
      .then(editado => {
        assert.exists(
          editado.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          editado.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          editado.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.exists(editado.email, 'Não retornou campo <email> na resposta.');
        assert.isArray(
          editado.assistirei,
          'Campo <assistirei> não é um array.',
        );
        assert.isArray(
          editado.assistidos,
          'Campo <assistidos> não é um array.',
        );
        assert.isArray(editado.favoritos, 'Campo <favoritos> não é um array.');
        assert.equal(
          editado.assistirei.length,
          0,
          'Campo <assistirei> não está iniciado vazio.',
        );
        assert.equal(
          editado.assistidos.length,
          0,
          'Campo <assistidos> não está iniciado vazio.',
        );
        assert.equal(
          editado.favoritos.length,
          0,
          'Campo <favoritos> do usuario a editar não está iniciado vazio.',
        );
        assert.equal(
          editado.email,
          usuario.email,
          'Email do usuario editado não confere.',
        );
      });
  });

  // eslint-disable-next-line no-undef
  it('confere a lista editada.', async () => {
    await banco.usuarios.leUsuarioPorId(criado._id).then(lido => {
      assert.exists(
        lido.assistirei,
        'Não retornou campo <assistirei> na resposta.',
      );
      assert.exists(
        lido.assistidos,
        'Não retornou campo <assistidos> na resposta.',
      );
      assert.exists(
        lido.favoritos,
        'Não retornou campo <favoritos> na resposta.',
      );
      assert.exists(lido.email, 'Não retornou campo <email> na resposta.');
      assert.isArray(lido.assistirei, 'Campo <assistirei> não é um array.');
      assert.isArray(lido.assistidos, 'Campo <assistidos> não é um array.');
      assert.isArray(lido.favoritos, 'Campo <favoritos> não é um array.');
      assert.isArray(lido.filmes, 'Campo <filmes> não é um array.');
      assert.equal(
        lido.assistirei.length,
        0,
        'Campo <assistirei> não está iniciado vazio.',
      );
      assert.equal(
        lido.assistidos.length,
        0,
        'Campo <assistidos> não está iniciado vazio.',
      );
      assert.equal(
        lido.favoritos.length,
        1,
        'Lista <favoritos> do usuario não contém 1 item.',
      );
      assert.equal(
        lido.favoritos[0],
        123,
        'O item nos favoritos não corresponde ao inserido.',
      );
      assert.equal(
        lido.filmes.length,
        0,
        'Campo <filmes> não está iniciado vazio.',
      );
      assert.equal(
        lido.email,
        usuario.email,
        'Email do usuario lido não confere.',
      );
    });
  });

  // eslint-disable-next-line no-undef
  it('edita o cache do usuário.', async () => {
    await banco.usuarios
      .editaUsuario(criado._id, 'filmes', [filme])
      .then(editado => {
        assert.exists(
          editado.assistirei,
          'Não retornou campo <assistirei> na resposta.',
        );
        assert.exists(
          editado.assistidos,
          'Não retornou campo <assistidos> na resposta.',
        );
        assert.exists(
          editado.favoritos,
          'Não retornou campo <favoritos> na resposta.',
        );
        assert.exists(editado.email, 'Não retornou campo <email> na resposta.');
        assert.isArray(
          editado.assistirei,
          'Campo <assistirei> não é um array.',
        );
        assert.isArray(
          editado.assistidos,
          'Campo <assistidos> não é um array.',
        );
        assert.isArray(editado.favoritos, 'Campo <favoritos> não é um array.');
        assert.equal(
          editado.assistirei.length,
          0,
          'Campo <assistirei> não está iniciado vazio.',
        );
        assert.equal(
          editado.assistidos.length,
          0,
          'Campo <assistidos> não está iniciado vazio.',
        );
        assert.equal(
          editado.favoritos[0],
          123,
          'O item nos favoritos não corresponde ao inserido.',
        );
        assert.equal(
          editado.filmes.length,
          0,
          'Campo <filmes> não está iniciado vazio.',
        );
        assert.equal(
          editado.email,
          usuario.email,
          'Email do usuario editado não confere.',
        );
      });
  });

  // eslint-disable-next-line no-undef
  it('confere o cache editado.', async () => {
    await banco.usuarios.leUsuarioPorId(criado._id).then(lido => {
      assert.exists(
        lido.assistirei,
        'Não retornou campo <assistirei> na resposta.',
      );
      assert.exists(
        lido.assistidos,
        'Não retornou campo <assistidos> na resposta.',
      );
      assert.exists(
        lido.favoritos,
        'Não retornou campo <favoritos> na resposta.',
      );
      assert.exists(lido.email, 'Não retornou campo <email> na resposta.');
      assert.isArray(lido.assistirei, 'Campo <assistirei> não é um array.');
      assert.isArray(lido.assistidos, 'Campo <assistidos> não é um array.');
      assert.isArray(lido.favoritos, 'Campo <favoritos> não é um array.');
      assert.isArray(lido.filmes, 'Campo <filmes> não é um array.');
      assert.equal(
        lido.assistirei.length,
        0,
        'Campo <assistirei> não está iniciado vazio.',
      );
      assert.equal(
        lido.assistidos.length,
        0,
        'Campo <assistidos> não está iniciado vazio.',
      );
      assert.equal(
        lido.favoritos.length,
        1,
        'Lista <favoritos> do usuario não contém 1 item.',
      );
      assert.equal(
        lido.favoritos[0],
        123,
        'O item nos favoritos não corresponde ao inserido.',
      );
      assert.equal(
        lido.filmes.length,
        1,
        'Campo <filmes> não está com 1 filme.',
      );
      assert.equal(
        lido.filmes[0].id,
        999,
        'O item no cache não corresponde ao inserido.',
      );
      assert.equal(
        lido.email,
        usuario.email,
        'Email do usuario lido não confere.',
      );
    });
  });

  // eslint-disable-next-line no-undef
  it('deleta o usuário criado.', async () => {
    await banco.usuarios.deletaUsuario(criado._id).then(lido => {
      assert.exists(
        lido.assistirei,
        'Não retornou campo <assistirei> na resposta.',
      );
      assert.exists(
        lido.assistidos,
        'Não retornou campo <assistidos> na resposta.',
      );
      assert.exists(
        lido.favoritos,
        'Não retornou campo <favoritos> na resposta.',
      );
      assert.exists(lido.email, 'Não retornou campo <email> na resposta.');
      assert.isArray(lido.assistirei, 'Campo <assistirei> não é um array.');
      assert.isArray(lido.assistidos, 'Campo <assistidos> não é um array.');
      assert.isArray(lido.favoritos, 'Campo <favoritos> não é um array.');
      assert.equal(
        lido.assistirei.length,
        0,
        'Campo <assistirei> não está iniciado vazio.',
      );
      assert.equal(
        lido.assistidos.length,
        0,
        'Campo <assistidos> não está iniciado vazio.',
      );
      assert.equal(
        lido.favoritos.length,
        1,
        'Lista <favoritos> do usuario não contém 1 item.',
      );
      assert.equal(
        lido.favoritos[0],
        123,
        'O item nos favoritos não corresponde ao inserido.',
      );
      assert.equal(
        lido.filmes.length,
        1,
        'Campo <filmes> não está com 1 filme.',
      );
      assert.equal(
        lido.filmes[0].id,
        999,
        'O item no cache não corresponde ao inserido.',
      );
      assert.equal(
        lido.email,
        usuario.email,
        'Email do usuario lido não confere.',
      );
    });
  });

  // eslint-disable-next-line no-undef
  it('confere usuário foi deletado.', async () => {
    await banco.usuarios.leUsuarioPorId(criado._id).then(lido => {
      assert.isString(lido, 'Não retornou um string.');
      assert.equal(
        lido,
        'Usuário não encontrado.',
        'Não retornou mensagem correta.',
      );
    });
  });
});
