import filmesApi from '../api/movieDB';
import * as banco from '../api/mongoDB';
import { filtraCamposFilmes } from '../utils/utils';

const excluirDaLista = (res, id, listas, listaDestino, filmeId) => {
  const dados = listas;

  dados[listaDestino] = dados[listaDestino].filter(
    item => parseInt(item, 10) !== filmeId,
  );

  return banco.usuarios
    .editaUsuario(id, listaDestino, dados[listaDestino])
    .then(usuario => {
      if (!usuario.email) {
        throw new Error(`Erro ao excluir ${filmeId} de ${listaDestino}.`);
      }
      const reuniaoSobreCache = [
        ...dados.favoritos,
        ...dados.assistidos,
        ...dados.assistirei,
      ];
      const alguemTaUsando = reuniaoSobreCache.find(
        item => parseInt(item, 10) === filmeId,
      );
      if (alguemTaUsando) return usuario;

      const cacheRemovido = dados.filmes.filter(
        item => parseInt(item.id, 10) !== filmeId,
      );
      return banco.usuarios.editaUsuario(id, 'filmes', cacheRemovido);
    })
    .catch(erro => erro);
};

const incluirNaLista = (res, id, listas, listaDestino, filmeId) => {
  const dados = listas;

  dados[listaDestino].push(filmeId);

  const cachePessoal = dados.filmes.find(
    item => parseInt(item.id, 10) === filmeId,
  );

  if (cachePessoal) {
    return banco.usuarios.editaUsuario(id, listaDestino, dados[listaDestino]);
  }

  return banco.filmes
    .leFilme(filmeId)
    .then(ret => {
      if (ret.id && parseInt(ret.id, 10) === filmeId) {
        const resposta = {
          ok: true,
          payload: ret,
        };
        return resposta;
      }
      return filmesApi(`movie/${filmeId}`);
    })
    .then(ret => {
      if (ret.ok) {
        const novoFilme = filtraCamposFilmes(ret.payload);
        dados.filmes.push(novoFilme);
        return banco.usuarios.editaUsuario(id, 'filmes', dados.filmes);
      }
      const { sme, cod } = ret.payload;
      const msg = `Erro ao incluir filme ${filmeId} em ${listaDestino}: [${cod}] ${sme}`;
      throw new Error(msg);
    })
    .then(usuario => {
      if (!usuario.email) {
        throw new Error(`Erro ao incluir filme ${filmeId} em ${listaDestino}.`);
      }
      return banco.usuarios.editaUsuario(id, listaDestino, dados[listaDestino]);
    })
    .catch(erro => erro);
};

export const manipulaLista = (req, res) => {
  const response = { ok: false, payload: null };

  const { usuario } = req;

  let listaDestino;
  const path = req.path.split('/')[1];
  switch (path) {
    case 'favoritar':
      listaDestino = 'favoritos';
      break;
    case 'assistidos':
      listaDestino = 'assistidos';
      break;
    case 'assistirei':
      listaDestino = 'assistirei';
      break;
    default:
      break;
  }

  if (!listaDestino) {
    return res.status(400).json(response);
  }

  req.assert('id', 'ID obrigatório').isString();

  const errors = req.validationErrors();

  if (errors) {
    response.payload = errors;
    return res.status(400).json(response);
  }

  const filmeId = parseInt(req.params.id, 10);

  if (!filmeId) {
    response.payload = {
      id: req.params.id,
      msg: 'ID inválido.',
    };
    return res.status(400).json(response);
  }

  const acao = req.query.a && req.query.a === 'exc' ? 'excluir' : 'incluir';
  const existeNestaLista = usuario[listaDestino].find(
    item => parseInt(item, 10) === filmeId,
  );

  if (acao === 'incluir' && existeNestaLista) {
    response.payload = {
      id: filmeId,
      msg: `Filme já existe na lista ${listaDestino}.`,
    };
    return res.status(400).json(response);
  }

  if (acao === 'excluir' && !existeNestaLista) {
    response.payload = {
      id: filmeId,
      msg: `Filme não existe na lista ${listaDestino}.`,
    };
    return res.status(400).json(response);
  }

  const listas = {
    favoritos: usuario.favoritos,
    assistidos: usuario.assistidos,
    assistirei: usuario.assistirei,
    filmes: usuario.filmes,
  };

  const manipularLista = new Promise((resolve, reject) => {
    if (acao === 'excluir') {
      resolve(excluirDaLista(res, usuario.id, listas, listaDestino, filmeId));
    } else if (acao === 'incluir') {
      resolve(incluirNaLista(res, usuario.id, listas, listaDestino, filmeId));
    }
    reject(new Error('Erro de manipulação da lista pessoal.'));
  });

  return manipularLista
    .then(usr => {
      if (!usr.email) {
        throw new Error(`Erro ao ${acao} filme ${filmeId} em ${listaDestino}.`);
      }
      response.ok = true;
      response.payload = {
        id: filmeId,
        msg: `Sucesso ao ${acao} na lista ${listaDestino}.`,
      };
      return res.status(200).json(response);
    })
    .catch(erro => {
      response.payload = {
        id: filmeId,
        msg: erro.message,
      };
      return res.status(400).json(response);
    });
};

export const obtemLista = (req, res) => {
  const response = { ok: false, payload: null };

  let listaDestino;
  const path = req.path.split('/')[1];
  switch (path) {
    case 'assistidos':
      listaDestino = 'assistidos';
      break;
    case 'assistirei':
      listaDestino = 'assistirei';
      break;
    default:
      break;
  }

  if (!listaDestino) {
    return res.status(400).json(response);
  }

  const { page = 1 } = req.query;
  const resp = {
    page,
    total_results: 0,
    total_pages: 1,
    results: [],
  };

  const ids = req.usuario[listaDestino];
  if (!ids.length) {
    response.ok = true;
    response.payload = resp;
    return res.status(200).json(response);
  }

  const lista = req.usuario.filmes.filter(item =>
    ids.find(i => parseInt(i, 10) === parseInt(item.id, 10)),
  );

  const inicio = (page - 1) * 20;
  const fim = inicio + 20;

  resp.page = page;
  resp.total_results = lista.length;
  resp.total_pages = Math.ceil(lista.length / 20);
  resp.results = lista.slice(inicio, fim);

  response.ok = true;
  response.payload = resp;
  return res.status(200).json(response);
};
