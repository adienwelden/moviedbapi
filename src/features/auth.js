import jwt from 'jsonwebtoken';

import serverConfig from '../../config';
import * as banco from '../api/mongoDB';

const decifraTokenAutenticacao = token =>
  new Promise((resolve, reject) => {
    if (!token) {
      reject(new Error('Acesso não autorizado.'));
    }
    if (!serverConfig.tokenSecret) {
      reject(new Error('Erro no token de segurança. Contate o administrador.'));
    }
    jwt.verify(token, serverConfig.tokenSecret, (erro, decifrado) => {
      if (erro) {
        let msg;
        switch (erro.name) {
          case 'TokenExpiredError':
            msg = 'Sua sessão expirou. Faça login novamente.';
            break;
          case 'JsonWebTokenError':
            msg = 'Erro de autenticação. Faça login novamente.';
            break;
          default:
            msg = 'Ocorreu um erro na autenticação.';
            break;
        }
        reject(new Error(msg));
      }
      if (!decifrado) {
        reject(new Error('Erro de autenticação.'));
      }
      resolve(decifrado);
    });
  });

export const autenticado = (req, res, next) => {
  const response = { ok: false, payload: null };
  const auth = req.headers.authorization;
  const token = (auth && auth.split(' ')[1]) || null;

  decifraTokenAutenticacao(token)
    .then(decifrado => {
      if (decifrado.id) {
        return banco.usuarios.leUsuarioPorId(decifrado.id);
      }
      throw new Error(decifrado.message);
    })
    .then(usuario => {
      if (usuario.email) {
        req.usuario = usuario.toJSON();
        next();
        return;
      }
      throw new Error('Erro na autenticação.');
    })
    .catch(erro => {
      response.payload = erro.message;
      res.status(401).json(response);
    });
};

const geraTokenAutenticacao = usuario =>
  new Promise((resolve, reject) => {
    if (!usuario) {
      reject(new Error('Ocorreu um erro na tentativa de autenticação.'));
    }
    const payload = {
      id: usuario.id,
    };
    jwt.sign(
      payload,
      serverConfig.tokenSecret,
      { expiresIn: '24h' },
      (erro, token) => {
        if (erro) {
          reject(new Error('Ocorreu um erro na tentativa de autenticação.'));
        }
        resolve(token);
      },
    );
  });

const autentica = (usuario, password) =>
  new Promise((resolve, reject) => {
    usuario.comparaSenhas(password, (erro, bateu) => {
      if (erro) {
        reject(new Error('Ocorreu um erro na tentativa de autenticação.'));
      }
      if (!bateu) {
        reject(new Error('Senha incorreta.'));
      }
      resolve(geraTokenAutenticacao(usuario.toJSON()));
    });
  });

export const login = (req, res) => {
  const response = { ok: false, payload: null };

  req.assert('email', 'Email inválido').isEmail();
  req.assert('email', 'Email obrigatório').notEmpty();
  req.assert('password', 'Senha obrigatória').notEmpty();
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  const errors = req.validationErrors();

  if (errors) {
    response.payload = errors;
    return res.status(400).json(response);
  }

  const credenciais = {
    email: req.body.email,
    password: req.body.password,
  };

  return banco.usuarios
    .leUsuarioPorEmail(credenciais.email)
    .then(usuario => {
      if (usuario.email && usuario.email === credenciais.email) {
        return autentica(usuario, credenciais.password);
      }
      const str = typeof usuario === 'string';
      const msg = str
        ? usuario
        : 'Ocorreu um erro na tentativa de autenticação.';
      throw new Error(msg);
    })
    .then(token => {
      if (token && typeof token === 'string') {
        response.ok = true;
        response.payload = {
          token,
        };
        res.status(200).json(response);
      } else {
        throw new Error('Ocorreu um erro na tentativa de autenticação.');
      }
    })
    .catch(erro => {
      response.payload = erro.message;
      res.status(400).json(response);
    });
};

export const cadastro = (req, res) => {
  const response = { ok: false, payload: null };

  req.assert('email', 'Email inválido').isEmail();
  req.assert('email', 'Email obrigatório').notEmpty();
  req.assert('password', 'Senha obrigatória').notEmpty();
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  const errors = req.validationErrors();

  if (errors) {
    response.payload = errors;
    return res.status(400).json(response);
  }

  const usuario = {
    email: req.body.email,
    password: req.body.password,
  };

  return banco.usuarios
    .criaUsuario(usuario)
    .then(ret => {
      if (ret) {
        const encontrado = ret.toJSON();
        delete encontrado.id;
        response.ok = true;
        response.payload = encontrado;
        res.status(200).json(response);
      } else {
        response.payload = 'Erro na criação do novo usuário.';
        throw new Error(response.payload);
      }
    })
    .catch(erro => {
      response.payload = erro.message;
      res.status(400).json(response);
    });
};

export const deletar = (req, res) => {
  const response = { ok: false, payload: null };

  const { usuario } = req;

  return banco.usuarios
    .deletaUsuario(usuario.id)
    .then(ret => {
      if (ret) {
        const encontrado = ret.toJSON();
        delete encontrado.id;
        response.ok = true;
        response.payload = encontrado;
        res.status(200).json(response);
      } else {
        throw new Error('Erro deletar usuário.');
      }
    })
    .catch(erro => {
      response.payload = erro.message;
      res.status(400).json(response);
    });
};
