import serverConfig from '../../config';
import filmesApi from '../api/movieDB';
import * as banco from '../api/mongoDB';
import * as utils from '../utils/utils';

const favoritos = (req, tamFeed) => {
  const ids = req.usuario.favoritos.slice(0, tamFeed);

  if (!ids.length) return Promise.resolve([]);

  const favo = req.usuario.filmes.filter(filme =>
    ids.find(filmeId => parseInt(filmeId, 10) === parseInt(filme.id, 10)),
  );

  return Promise.resolve(favo);
};

const buscaLancamentos = (req, tamFeed) => {
  const queryPadrao = {
    language: 'pt-BR',
    include_adult: false,
  };

  const queries = Object.assign(queryPadrao, req.query);

  return filmesApi('movie/now_playing', queries)
    .then(ret => {
      if (ret.ok) {
        return ret.payload.results.slice(0, tamFeed);
      }
      const { sme, cod } = ret.payload;
      const msg = `(Lançamentos) Erro: [${cod}] ${sme}`;
      throw new Error(msg);
    })
    .catch(erro => erro);
};

const buscaGenero = (req, tamFeed, generoId) => {
  let com = '';
  let sem = '';

  com = `${generoId}`;
  sem = serverConfig.categoriasOrdem.filter(item => item !== generoId);

  const queryPadrao = {
    language: 'pt-BR',
    include_adult: false,
  };

  const queries = Object.assign(queryPadrao, req.query);
  queries.with_genres = com;
  queries.without_genres = sem.toString();

  const genero = Object.keys(utils.categoriasFilme).find(
    item => utils.categoriasFilme[item] === generoId,
  );

  let fazerCache = false;

  return banco.filmes
    .leFilmes(genero)
    .then(ret => {
      if (Array.isArray(ret.docs) && ret.docs.length) {
        const resposta = {
          ok: true,
          payload: {
            results: ret.docs,
          },
        };
        return resposta;
      }
      fazerCache = true;
      return filmesApi('discover/movie', queries);
    })
    .then(ret => {
      if (ret.ok) {
        if (fazerCache) {
          const filmes = ret.payload.results.map(item => {
            const filme = utils.filtraCamposFilmes(item);
            filme.genero = genero;
            return filme;
          });
          banco.filmes.criaFilmes(filmes);
        }
        return ret.payload.results.slice(0, tamFeed);
      }
      const { sme, cod } = ret.payload;
      const msg = `(${genero}) Erro: [${cod}] ${sme}`;
      throw new Error(msg);
    })
    .catch(erro => erro);
};

// eslint-disable-next-line import/prefer-default-export
export const buscaFeed = (req, res) => {
  const response = { ok: false, payload: null };

  let { categ_size: tamFeed = serverConfig.tamanhoFeed } = req.query;
  tamFeed = parseInt(tamFeed, 10) ? tamFeed : serverConfig.tamanhoFeed;
  tamFeed = tamFeed > 0 ? tamFeed : serverConfig.tamanhoFeed;

  const feedFavoritos = favoritos(req, tamFeed);
  const feedLancamentos = buscaLancamentos(req, tamFeed);
  const feedGeneros = serverConfig.categoriasOrdem.map(categ =>
    buscaGenero(req, tamFeed, categ),
  );
  const buscaDeCategorias = [feedFavoritos, feedLancamentos, ...feedGeneros];

  return Promise.all(buscaDeCategorias)
    .then(todasListas => {
      let feed = [];
      todasListas.map(lista => {
        feed = [...feed, ...lista];
        return null;
      });
      response.ok = true;
      response.payload = feed;
      return res.status(200).json(response);
    })
    .catch(erro => {
      response.payload = erro.message;
      return res.status(400).json(response);
    });
};
