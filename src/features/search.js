import filmesApi from '../api/movieDB';

// eslint-disable-next-line import/prefer-default-export
export const buscaTituloOuAutor = (req, res) => {
  const response = { ok: false, payload: null };

  req.assert('query', 'Termo da busca é obrigatório.').exists();
  req.assert('query', 'Termo da busca é obrigatório.').isString();
  req
    .assert('query', 'Termo da busca não pode ser vazio.')
    .notEmpty()
    .trim()
    .escape();

  const errors = req.validationErrors();

  if (errors) {
    response.payload = errors;
    return res.status(400).json(response);
  }

  const queryPadrao = {
    language: 'pt-BR',
    include_adult: false,
  };

  const queries = Object.assign(queryPadrao, req.query);

  return filmesApi('search/multi', queries)
    .then(ret => {
      if (ret.ok) {
        return res.status(200).json(ret);
      }
      const { sme, cod } = ret.payload;
      const msg = `(Busca) Erro: [${cod}] ${sme}`;
      throw new Error(msg);
    })
    .catch(erro => {
      response.payload = erro.message;
      res.status(400).json(response);
    });
};
