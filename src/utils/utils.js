/* eslint-disable camelcase */
export const categoriasFilme = {
  acao: 12,
  aventura: 28,
  terror: 27,
  drama: 18,
  animacao: 16,
  comedia: 35,
  crime: 80,
  documentario: 99,
  familia: 10751,
  fantasia: 14,
  historia: 36,
  musical: 10402,
  misterio: 9648,
  romance: 10749,
  ficcao: 878,
  tv_movie: 10770,
  thriller: 53,
  guerra: 10752,
  faroeste: 37,
};

export const filtraCamposFilmes = campos => {
  const {
    adult,
    backdrop_path,
    id,
    original_language,
    original_title,
    overview,
    popularity,
    release_date,
    title,
    video,
    vote_average,
    vote_count,
  } = campos;

  const genresIds = campos.genre_ids
    ? campos.genre_ids
    : campos.genres.map(g => parseInt(g.id, 10));

  const filme = {
    adult,
    backdrop_path,
    genre_ids: genresIds,
    id,
    original_language,
    original_title,
    overview,
    popularity,
    release_date,
    title,
    video,
    vote_average,
    vote_count,
  };
  return filme;
};

// para os testes:
export const filmes = [123, 124, 125, 126, 127, 128, 129];
