import { Router } from 'express';

import auth from '../controllers/auth';
import * as filmesController from '../controllers/movies';

const router = new Router();

router.route('/feed').get(auth.autenticado, filmesController.feed.buscaFeed);

router
  .route('/buscar')
  .get(auth.autenticado, filmesController.busca.buscaTituloOuAutor);

router
  .route('/favoritar/:id')
  .get(auth.autenticado, filmesController.listas.manipulaLista);

router
  .route('/assistirei/:id')
  .get(auth.autenticado, filmesController.listas.manipulaLista);

router
  .route('/assistidos/:id')
  .get(auth.autenticado, filmesController.listas.manipulaLista);

router
  .route('/assistirei')
  .get(auth.autenticado, filmesController.listas.obtemLista);

router
  .route('/assistidos')
  .get(auth.autenticado, filmesController.listas.obtemLista);

export default router;
