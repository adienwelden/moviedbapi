import { Router } from 'express';

import auth from '../controllers/auth';

const router = new Router();

router.route('/cadastro').post(auth.cadastro);

router.route('/cadastro').delete(auth.autenticado, auth.deletar);

router.route('/login').post(auth.login);

export default router;
