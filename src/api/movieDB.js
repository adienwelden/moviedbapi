import axios from 'axios';
import queryString from 'query-string';

import serverConfig from '../../config';

const api = (endpoint, params = {}) => {
  const response = { ok: false, payload: null };

  const host = serverConfig.movieDB;
  const qs = queryString.stringify(params);
  const config = {
    url: `${host}/${endpoint}?api_key=${serverConfig.movieDBApiKey}&${qs}`,
    method: 'get',
    responseType: 'json',
    timeout: 5000,
  };

  return axios(config)
    .then(res => {
      response.ok = true;
      response.payload = res.data;
      return response;
    })
    .catch(erro => {
      response.payload = {
        msg: 'Ocorreu uma falha no servidor.',
        cod:
          erro.response && erro.response.data
            ? erro.response.data.status_code
            : erro.code,
        sme:
          erro.response && erro.response.data
            ? erro.response.data.status_message
            : erro.message,
      };
      return response;
    });
};

export default api;
