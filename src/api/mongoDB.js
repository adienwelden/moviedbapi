import mongoose from 'mongoose';

import * as usuarios from '../repos/user';
import * as filmes from '../repos/movie';

import serverConfig from '../../config';

mongoose.set('useFindAndModify', false);

mongoose.connect(
  serverConfig.mongoDB,
  {
    useNewUrlParser: true,
  },
  () => {},
  () => {},
);

export { usuarios, filmes };
