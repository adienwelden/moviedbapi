import mongoose from 'mongoose';

import Filmes from '../models/movie';

export const criaFilme = filme =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Filmes.findOne({ id: filme.id }, (erro, duplicado) => {
      if (erro) {
        reject(new Error('Erro ao salvar filme.'));
      } else if (duplicado) {
        resolve('Filme ja existe.');
      }
      const instancia = new Filmes(filme);
      instancia.save((err, salvo) => {
        if (err) reject(new Error('Erro ao salvar filme.'));
        resolve(salvo);
      });
    });
  });

export const criaFilmes = filmes =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Filmes.insertMany(filmes, (erro, docs) => {
      if (erro || !docs) {
        reject(new Error('Erro ao salvar filmes.'));
      }
      resolve(docs);
    });
  });

export const leFilme = id =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Filmes.findOne({ id }, (erro, encontrado) => {
      if (erro) {
        reject(new Error('Erro ao buscar filme.'));
      } else if (!encontrado) {
        resolve('Filme não encontrado.');
      }
      resolve(encontrado);
    });
  });

export const leFilmes = (genero = null, pagina = 1) =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    const criterio = genero ? { genero } : {};
    Filmes.paginate(
      criterio,
      { page: pagina, limit: 20 },
      (erro, encontrados) => {
        if (erro) {
          reject(new Error('Erro ao buscar filmes no banco.'));
        }
        resolve(encontrados);
      },
    );
  });

export const leFilmesIds = (ids, pagina = 1) =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Filmes.paginate(
      { id: { $in: ids } },
      { page: pagina, limit: 20 },
      (erro, encontrados) => {
        if (erro) {
          reject(new Error('Erro ao buscar filmes no banco.'));
        }
        resolve(encontrados);
      },
    );
  });

export const deletaFilmeId = id =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Filmes.deleteOne({ id }, erro => {
      if (erro) {
        reject(new Error('Erro ao deletar filme.'));
      }
      resolve('Filme foi deletado.');
    });
  });

export const deletaTodosFilmes = () =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Filmes.deleteMany({}, erro => {
      if (erro) {
        reject(new Error('Erro ao deletar filmes.'));
      }
      resolve('Todos filmes foram deletados.');
    });
  });
