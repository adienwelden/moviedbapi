import mongoose from 'mongoose';

import Usuarios from '../models/user';

export const criaUsuario = usuario =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Usuarios.findOne({ email: usuario.email }, (erro, duplicado) => {
      if (erro) {
        reject(new Error('Erro ao salvar usuário.'));
      } else if (duplicado) {
        reject(new Error('Email já está cadastrado.'));
      }
      const instancia = new Usuarios(usuario);
      instancia.save((err, salvo) => {
        if (err) reject(new Error('Erro ao salvar usuário.'));
        resolve(salvo);
      });
    });
  });

export const leUsuarioPorId = id =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Usuarios.findById(id, (erro, encontrado) => {
      if (erro) {
        reject(new Error('Erro ao buscar usuário.'));
      } else if (!encontrado) {
        resolve('Usuário não encontrado.');
      }
      resolve(encontrado);
    });
  });

export const leUsuarioPorEmail = email =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Usuarios.findOne({ email }, (erro, encontrado) => {
      if (erro) {
        reject(new Error('Erro ao buscar usuário.'));
      } else if (!encontrado) {
        resolve('Usuário não encontrado.');
      }
      resolve(encontrado);
    });
  });

export const editaUsuario = (id, campo, valor) =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Usuarios.findByIdAndUpdate(id, { [campo]: valor }, (erro, encontrado) => {
      if (erro) {
        reject(new Error('Erro ao editar usuário.'));
      } else if (!encontrado) {
        reject(new Error('Usuário não encontrado.'));
      }
      resolve(encontrado);
    });
  });

export const deletaUsuario = id =>
  new Promise((resolve, reject) => {
    if (!mongoose.connection.readyState) {
      reject(new Error('Banco indisponível.'));
    }
    Usuarios.findByIdAndDelete(id, (erro, encontrado) => {
      if (erro) {
        console.log('findByIdAndDelete', erro);
        reject(new Error('Erro ao deletar usuário.'));
      } else if (!encontrado) {
        reject(new Error('Usuário não encontrado.'));
      }
      resolve(encontrado);
    });
  });
