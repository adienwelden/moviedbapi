/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
import mongoose from 'mongoose';

const filmeSchema = new mongoose.Schema(
  {
    genero: { type: String, default: '' },
    adult: Boolean,
    backdrop_path: String,
    genre_ids: [Number],
    id: Number,
    original_language: String,
    original_title: String,
    overview: String,
    popularity: String,
    release_date: String,
    title: String,
    video: Boolean,
    vote_average: String,
    vote_count: Number,
  },
  {
    toObject: {
      transform: (doc, ret) => {
        delete ret._id;
        delete ret.__v;
      },
    },
    toJSON: {
      transform: (doc, ret) => {
        delete ret._id;
        delete ret.__v;
      },
    },
  },
);

export default filmeSchema;
