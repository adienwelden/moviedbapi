/* eslint-disable no-underscore-dangle */
import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

import usuarioSchema from './userSchema';

function preSave(next) {
  bcrypt.genSalt(10, (errSalt, salt) => {
    bcrypt.hash(this.password, salt, (errHash, hash) => {
      this.password = hash;
      next();
    });
  });
}

usuarioSchema.pre('save', preSave);

function comparaSenhas(password, cb) {
  bcrypt.compare(password, this.password, (erro, bateu) => {
    cb(erro, bateu);
  });
}

usuarioSchema.methods.comparaSenhas = comparaSenhas;

function toJSON() {
  const instancia = this;
  const usuarioObj = instancia.toObject({ versionKey: false });
  usuarioObj.id = usuarioObj._id;
  delete usuarioObj._id;
  delete usuarioObj.password;
  return usuarioObj;
}

usuarioSchema.methods.toJSON = toJSON;

const Usuario = mongoose.model('Usuario', usuarioSchema);

export default Usuario;
