import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

import filmeSchema from './movieSchema';

filmeSchema.plugin(mongoosePaginate);

const Filme = mongoose.model('Filme', filmeSchema);

export default Filme;
