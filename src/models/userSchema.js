import mongoose from 'mongoose';

import filmeSchema from './movieSchema';

const usuarioSchema = new mongoose.Schema({
  email: String,
  password: String,
  assistirei: [String],
  assistidos: [String],
  favoritos: [String],
  filmes: [filmeSchema],
});

export default usuarioSchema;
